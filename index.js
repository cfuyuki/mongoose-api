var express = require('express')
var mongoose = require('mongoose')

// Middleware
var bodyParser = require('body-parser')
var logger = require('morgan')
var errorHandler = require('errorhandler')

// Setup express and mongodb connection through mongoose
var app = express()
var dbUri = 'mongodb://localhost:27017/api'
var dbConnection = mongoose.createConnection(dbUri);
var Schema = mongoose.Schema

// Predefined Values and Functions
var enumRoles = ['user', 'admin', 'staff']
var positiveNum = function(value) {
    if (value < 0) {
        return false
    } else {
        return true
    }
}

// Schema
var postSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        match: /^([\w ,.!?]{1,100})$/,
        set: function(value) {
            return value.toUpperCase()
        },
        get: function(value) {
            return value.toLowerCase()
        }
    },
    text: {
        type: String,
        maxlength: 2000
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    followers: [Schema.Types.ObjectId],
    meta: Schema.Types.Mixed,
    comments: [{
        text: {
            type: String,
            trim: true,
            maxlength: 1000,
        },
        author: {
            id: {
                type: Schema.Types.ObjectId,
                ref: 'User'
            },
            name: String,
            role: {
                type: String,
                enum: enumRoles
            }
        }
    }],
    viewCounter: {
        type: Number
    },
    published: Boolean,
    createdAt: {
        type: Date,
        default: Date.now,
        required: true
    },
    updatedAt: {
        type: Date,
        default: Date.now,
        required: true
    }
})

var userSchema = new Schema({
    name: String,
    role: {
        type: String,
        enum: enumRoles
    }
})

//Schema Functions
postSchema.path('viewCounter').validate(positiveNum)
postSchema.virtual('hasComments').get(function() {
        return this.comments.length > 0
    })
    //Mongoose Middleware
postSchema.pre('save', function(next) {
    this.updatedAt = new Date()
    next()
})
postSchema.pre('validate', function(next) {
    var error = null
    if (this.isModified('comments') && this.comments.length > 0) {
        this.comments.forEach(function(value, key, list) {
            if (!value.text || !value.author.id) {
                error = new Error('Text and author for a comment are both required!')
            }
        })
    }
    if (error) return next(error)
    next()
})
postSchema.post('save', function(document) {
    console.log('Object was saved!')
})

// Define Custom Methods
// Static Method
postSchema.statics.staticMethod = function(callback){
    console.log('static method')
    return callback()
}
// Instance Method
postSchema.methods.myMethod = function(callback){
    console.log('my instance method')
    return callback()
}

// Create Models
var Post = dbConnection.model('Post', postSchema, 'posts') // Model, Schema, Collection
var User = dbConnection.model('User', userSchema, 'users') // Model, Schema, Collection

// Use Middleware in all routes
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
        extended: true
    })) // extended: true > nested object can be parsed 

//  Post Route GET
app.get('/', function(req, res) {
    Post.staticMethod(function(){
        res.send('ok')        
    })
})

app.get('/posts', function(req, res, next) {
    Post.find({}, 'id title createdAt', {
        limit: 10,
        sort: {
            createdAt: -1
        }
    }, function(error, posts) {
        if (error) return next(error)
        res.send(posts)
    })
})

app.get('/posts/:id', function(req, res, next) {
    Post.findOne({
        _id: req.params.id
    }).populate('author').exec(function(error, post) {
        if (error) return next(error)
        post.myMethod(function(){})
        res.send(post.toJSON({
            getters: true,
            virtuals: true
        }))
    })
})

// Post Route POST and create mongoose's save method with Middleware bodyParser (req.body)
app.post('/posts', function(req, res, next) {
    var post = new Post(req.body)
    post.validate(function(error) {
        console.log(error)
        if (error) return next(error)
        post.save(function(error, results) {
            if (error) return next(error)
            res.send(results)
        })
    })
})

// Post Route PUT
app.put('/posts/:id', function(req, res, next) {
    Post.findOne({
        _id: req.params.id
    }, function(error, post) {
        if (error) return next(error)
        post.set(req.body)
        post.save(function(error, results) {
            if (error) return next(error)
            res.send(results.toJSON({
                getters: true,
                virtuals: true
            }))
        })
    })
})

// Post Route DELETE
app.delete('/posts/:id', function(req, res, next) {
    Post.findOne({
        _id: req.params.id
    }, function(error, post) {
        if (error) return next(error)
        post.remove(function(error, results) {
            if (error) return next(error)
            res.send(results)
        })
    })
})

// User Route POST
app.post('/users', function(req, res, next) {
    var user = new User(req.body)
    user.save(function(error, results) {
        if (error) return next(error)
        res.send(results)
    })
})


// Use error handler middleware
app.use(errorHandler())

var server = require('http').createServer(app).listen(3000)
