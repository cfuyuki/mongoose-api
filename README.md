# README #

### Summary ###

**REST API with Node.js, Express.js, and MongoDB(Mongoose) where its primary focus is on versatile usages of Mongoose.** This was introduced by Azat Mardan on the Udemy Course "[Mongoose: Master Object-Document Mapper for Node.js and Express.js](https://www.udemy.com/mongoose/)". It does not contain any client codes but server side codes specifically with Mongoose for HTTP request and response.

**See [Wiki](https://bitbucket.org/cfuyuki/mongoose-api/wiki/Home) for More Details**

### Prerequisites ###

You need to have those before installation:

* Node.js
* MongoDB

### Installation ###

1: Clone or download this repository.

2: Install the Node.js dependencies:

```
#!javascript

$ npm install
```

3: Run the app:

```
#!javascript

$ node index.js
```

### Dependencies ###

* [Mongoose](http://mongoosejs.com/)
* [Express 4.x](http://expressjs.com/)
* [Express bodyParser](https://github.com/expressjs/body-parser)
* [Express Morgan](https://github.com/expressjs/morgan)

Optional:

* [Express errorhandler](https://github.com/expressjs/errorhandler)
* [Node Okay](https://github.com/brianc/node-okay)

### Database ###

This application uses the next MongoDB configuration. You may change any of those in `index.js`.

* Database name: api
* Collection name: posts 

To bulk-import sample data from "sampleData.json" to local MongoDB installation,
in CLI, change directory to the app folder and run:

```
mongoimport -d api -c posts --jsonArray < sampleData.json
```